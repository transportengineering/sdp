{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE FlexibleContexts                  #-}
{-# LANGUAGE TypeFamilies                  #-}
{-# LANGUAGE ViewPatterns                  #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PatternSynonyms            #-}
{-# LANGUAGE TypeInType                 #-}
{-# LANGUAGE TypeOperators              #-}
module SDP where
import           Control.Applicative
import qualified Data.Attoparsec.ByteString  as A
import qualified Data.Attoparsec.ByteString.Char8  as A8
import           Data.Attoparsec.Text.Symbol
import qualified Data.Attoparsec.Text.Symbol as S
import           Data.ByteString             (ByteString)
import           Data.ByteString.Builder     (Builder)
import qualified Data.ByteString.Builder     as B
import           Data.ByteString.Short       (ShortByteString)
import            qualified Data.ByteString.Short       as S
import           Data.Char                   (isAlphaNum)
import           Data.Foldable               (foldMap)
import           Data.Kind
import           Data.DList (DList)
import qualified Data.DList as DL
import           Data.List                   (intersperse)
import           Data.Semigroup
import           Data.String
import           Data.Text                   (Text)
import qualified Data.Text                   as T
import qualified Data.Text.Encoding          as TE
import           Data.Time
import           Data.Time.Clock.POSIX       (POSIXTime)
import           Data.Word
import           GHC.TypeLits

data NetType = IN
  deriving (Show, Eq)

data AddrType = IP4 | IP6
  deriving (Show, Eq)

newtype Alphanumeric = Alphanumeric ShortByteString
  deriving (Eq, Monoid, Show, Read, Ord, Semigroup)

instance IsString Alphanumeric where
  fromString = fromString . filter isAlphaNum

-- | Section 5.2
data Origin = Origin
  { originUsername       :: !Text
  , originSessionId      :: !Word
  , originSessionVersion :: !Word
  , originNetType        :: !NetType
  , originAddrType       :: !AddrType
  } deriving (Show, Eq)

-- | Section 5.7
data ConnectionData = ConnectionData
  { connectionNetType  :: !NetType
  , connectionAddrType :: !AddrType
  , connectionAddress  :: !Text
  } deriving (Show, Eq)

data BwType = CT | AS
  deriving (Show, Eq)

-- | Section 5.8
data Bandwidth = Bandwidth
  { bandwidthType  :: !BwType
  , bandwidthValue :: !Word -- ^ kilobits/s
  } deriving (Show, Eq)

newtype NTPTime = NTPTime Integer
  deriving (Num, Integral, Real, Show, Eq, Ord, Enum)

ntpToPOSIXTime :: NTPTime -> POSIXTime
ntpToPOSIXTime (NTPTime i) = fromInteger (i - 2208988800)

-- | Section 5.9
data Timing = Timing
  { startTime   :: !NTPTime
  , stopTime    :: !NTPTime
  , repeatTimes :: ![RepeatTimes] -- ^ r=
  } deriving (Show, Eq)

-- | Section 5.10
data RepeatTimes = RepeatTimes
  { repeatInterval             :: !NominalDiffTime
  , repeatActiveDuration       :: !NominalDiffTime
  , repeatOffsetsFromStartTime :: !NominalDiffTime
  } deriving (Show, Eq)

-- | Section 5.11
data Timezones = Timezones
  { tzAdjustment0 :: !NTPTime
  , tzOffset0     :: !NominalDiffTime
  , tzAdjustment1 :: !NTPTime
  , tzOffset1     :: !NominalDiffTime
  } deriving (Eq, Show)

-- | Section 5.12
data EncryptionKeys
  = Clear !ShortByteString
  | Base64 !ShortByteString
  | URI !Text
  | Prompt
  deriving (Eq, Show)

data Orientation
  = Portrait
  | Landscape
  | Seascape
  deriving (Eq, Show)

data ConferenceType
  = Broadcast
  | Meeting
  | Moderated
  | Test
  | H332
  deriving (Eq, Show)

data Attribute
  = Recvonly
  | Sendrecv
  | Sendonly
  | Inactive
  | Orient !Orientation
  | Type !ConferenceType
  | Charset !ShortByteString -- ^ a US-ASCII string specifying e.g. "ISO-10646"
  | SdpLang !ShortByteString -- ^ a US-ASCII string specifying e.g. "en-US"
  | Framerate !Rational
  | Quality !Integer
  | Fmtp !Text ![Text]
    -- | Attribute values are octet strings, and may use any octet value except
    -- 0x00, 0x0A, and 0x0D
  | ValueAttr !Text !ShortByteString
  deriving (Eq, Show)

data MediaType
  = Audio
  | Video
  | Text
  | Application
  | Message
  deriving (Eq, Show)

data TransportProtocol
  = UDP
  | RTP_AVP
  | RTP_SAVP
  deriving (Eq, Show)

data MediaDescription = MediaDescription
  { mediaType   :: !MediaType
  , mediaPort   :: !Word
  , mediaProto  :: !TransportProtocol
  , mediaFormat :: [Text]
  } deriving (Eq, Show)

data SessionDescription = SessionDescription
  { protocolVersion     :: !Word -- ^ v=
  , origin              :: !Origin -- ^ o=
  , sessionName         :: !Text -- ^ s=
  , sessionInformation  :: !(Maybe Text) -- ^ i=
  , sessionURI          :: !(Maybe Text) -- ^ u=
  , sessionEmail        :: ![Text] -- ^ e=
  , sessionPhone        :: ![Text] -- ^ p=
  , connectionData      :: !ConnectionData -- ^ c=
  , bandwidth           :: !(Maybe Bandwidth) -- ^ b=
  , timing              :: !Timing -- ^ t= and r=
  , timezoneAdjustments :: !Timezones -- ^ z=
  , encryptionKeys      :: !EncryptionKeys -- ^ k=
  , attributes          :: ![Attribute] -- ^ a=
  , mediaDescriptions   :: ![MediaDescription] -- ^ m=
  } deriving (Eq, Show)

spaced :: [Builder] -> Builder
spaced = mconcat . intersperse " "

encodeNetType :: NetType -> Builder
encodeNetType IN = "IN"

encodeAddrType :: AddrType -> Builder
encodeAddrType n = case n of
  IP4 -> "IP4"
  IP6 -> "IP6"

encodeOrigin :: Origin -> Builder
encodeOrigin Origin{ originUsername, originSessionId, originSessionVersion
                   , originNetType, originAddrType } = spaced
  [ TE.encodeUtf8Builder originUsername
  , B.wordDec originSessionId
  , B.wordDec originSessionVersion
  , encodeNetType originNetType
  , encodeAddrType originAddrType
  ]

encodeConnectionData :: ConnectionData -> Builder
encodeConnectionData ConnectionData{ connectionNetType, connectionAddrType
                                   , connectionAddress} = spaced
  [ encodeNetType connectionNetType
  , encodeAddrType connectionAddrType
  , TE.encodeUtf8Builder connectionAddress
  ]

encodeBandwidth :: Bandwidth -> Builder
encodeBandwidth Bandwidth{bandwidthType, bandwidthValue} =
  (case bandwidthType of
     CT -> "CT:"
     AS -> "AS:") <> B.wordDec bandwidthValue

encodeRepeatTimes :: RepeatTimes -> Builder
encodeRepeatTimes RepeatTimes{ repeatInterval, repeatActiveDuration
                             , repeatOffsetsFromStartTime} =
  spaced
  [ B.integerDec (floor repeatInterval)
  , B.integerDec (floor repeatActiveDuration)
  , B.integerDec (floor repeatOffsetsFromStartTime)
  ]

encodeTiming :: Timing -> Builder
encodeTiming Timing{startTime, stopTime, repeatTimes} =
  param "t" (spaced (map (B.integerDec . toInteger) [startTime, stopTime])) <>
  foldMap (param "r") (map encodeRepeatTimes repeatTimes)

param :: Builder -> Builder -> Builder
param k v = k <> "=" <> v <> "\r\n"

encode :: SessionDescription -> Builder
encode sd = mconcat
  [ param "v" (B.wordDec (protocolVersion sd))
  , param "o" (encodeOrigin (origin sd))
  , param "s" (TE.encodeUtf8Builder (sessionName sd))
  , foldMap (param "i" . TE.encodeUtf8Builder) (sessionInformation sd)
  , foldMap (param "u" . TE.encodeUtf8Builder) (sessionURI sd)
  , foldMap (param "e" . TE.encodeUtf8Builder) (sessionEmail sd)
  , foldMap (param "p" . TE.encodeUtf8Builder) (sessionPhone sd)
  , param "c" (encodeConnectionData (connectionData sd))
  , foldMap (param "b" . encodeBandwidth) (bandwidth sd)
  , encodeTiming (timing sd)
  ]
